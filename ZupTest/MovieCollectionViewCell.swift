//
//  MovieCollectionViewCell.swift
//  ZupTest
//
//  Created by Rodolfo Antonici on 6/30/16.
//  Copyright © 2016 Rodolfo Antonici. All rights reserved.
//
// This cell is based on the implementation of the InspirationCell
// https://www.raywenderlich.com/99087/swift-expanding-cells-ios-collection-views

import UIKit
import Kingfisher


class MovieCollectionViewCell: UICollectionViewCell {
    @IBOutlet private weak var imageView: UIImageView!
    @IBOutlet private weak var imageCoverView: UIView!
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var yearLabel: UILabel!
    
    var movie: Movie? {
        didSet {
            if let movie = self.movie {
                self.titleLabel.text = movie.title
                self.yearLabel.text = "\(movie.year)"
                if let posterURLString = movie.posterURLString, let url = NSURL(string:posterURLString) {
                    
                    self.imageView.kf_setImageWithURL(url, placeholderImage: nil, optionsInfo: nil, progressBlock: nil, completionHandler: { (image, error, cacheType, imageURL) in
                        
                    })
                }
            }
        }
    }
    
    override func applyLayoutAttributes(layoutAttributes: UICollectionViewLayoutAttributes) {
        super.applyLayoutAttributes(layoutAttributes)
        
        let featuredHeight = UltravisualLayoutConstants.Cell.featuredHeight
        let standardHeight = UltravisualLayoutConstants.Cell.standardHeight
        
        let delta = 1 - ((featuredHeight - CGRectGetHeight(frame)) / (featuredHeight - standardHeight))
        
        let minAlpha: CGFloat = 0.3
        let maxAlpha: CGFloat = 0.75
        
        imageCoverView.alpha = maxAlpha - (delta * (maxAlpha - minAlpha))
        
        let scale = max(delta, 0.5)
        self.titleLabel.transform = CGAffineTransformMakeScale(scale, scale)
        self.yearLabel.alpha = delta
    }
    

}
