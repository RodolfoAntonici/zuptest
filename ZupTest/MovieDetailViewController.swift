//
//  MovieDetailViewController.swift
//  ZupTest
//
//  Created by Rodolfo Antonici on 6/30/16.
//  Copyright © 2016 Rodolfo Antonici. All rights reserved.
//

import UIKit
import RealmSwift
import Kingfisher

class MovieDetailViewController: UIViewController {
    @IBOutlet weak var backgroundImageView: UIImageView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var yearLabel: UILabel!

    var movie: Movie?
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.clearBackground()
        self.tableView.rowHeight = UITableViewAutomaticDimension;
        self.tableView.estimatedRowHeight = 44.0
        
        if let movie = self.movie {
            if let posterURLString = movie.posterURLString, let url = NSURL(string:posterURLString) {
                self.backgroundImageView.kf_setImageWithURL(url, placeholderImage: nil, optionsInfo: nil, progressBlock: nil, completionHandler: { (image, error, cacheType, imageURL) in
                    
                })
            }
            self.titleLabel.text = movie.title
            self.yearLabel.text = "\(movie.year)"
            self.tableView.contentInset = UIEdgeInsets(top: 200, left: 0, bottom: 0, right: 0)

            
            MoviesManager.sharedManager.getMovieWithImdbID(movie.imdbID, completionHandler: { (searchResult) in
                self.movie = searchResult
                self.tableView.reloadData()
                
                
                
                }, errorHandler: { (error) in
                    
            })
            
        }
        

    }

    @IBAction func addAsFavorite(sender: AnyObject) {
        if let movie = self.movie {
            
            let realm = try! Realm()
            try! realm.write {
                realm.add(movie)
            }
        }
    }
}


extension MovieDetailViewController: UITableViewDataSource {
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        guard let movie = self.movie else {
            return UITableViewCell()
        }
        
        if indexPath.row == 0{
            let plotCell = tableView.dequeueReusableCellWithIdentifier("PlotCell") as! PlotCellTableViewCell
            
            plotCell.titleLabel.text = "Plot:"
            if let plot = movie.plot {
                plotCell.detailLabel.text = plot
            }
            return plotCell
        }
        else {
            let detailCell = tableView.dequeueReusableCellWithIdentifier("DetailCell") as! DetailTableViewCell
            detailCell.detailLabel.text = "N/A"
            switch indexPath.row {
            case 1:
                detailCell.titleLabel.text = "Genre:"
                if let genre = movie.genre {
                    detailCell.detailLabel.text = genre
                }
                
            case 2:
                detailCell.titleLabel.text = "Director"
                if let director = movie.director {
                    detailCell.detailLabel.text = director
                }
                
            case 3:
                detailCell.titleLabel.text = "Actors"
                if let actors = movie.actors {
                    detailCell.detailLabel.text = actors
                }
            default:
                break
            }
            return detailCell
            
        }
    }
}

//dynamic var runtime: String?
//dynamic var genre: String?
//dynamic var director: String?
//dynamic var actors: String?
//dynamic var plot: String?
class PlotCellTableViewCell: UITableViewCell {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var detailLabel: UILabel!
    
}

class DetailTableViewCell: UITableViewCell {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var detailLabel: UILabel!
    
}
