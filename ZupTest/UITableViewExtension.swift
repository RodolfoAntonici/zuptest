//
//  UITableViewExtension.swift
//  Jeitto
//
//  Created by Rodolfo Antonici on 24/4/16.
//  Copyright © 2016 Jeitto. All rights reserved.
//

import UIKit

extension UITableView {
    func clearBackground() {
        if self.backgroundView == nil {
            self.backgroundView = UIView()
        }
        
        self.backgroundView?.backgroundColor = UIColor.clearColor()
        self.backgroundColor = UIColor.clearColor()

    }
    
    
}

extension UITableViewCell {
    func clearBackground() {
        if self.backgroundView == nil {
            self.backgroundView = UIView()
        }
        
        self.backgroundView?.backgroundColor = UIColor.clearColor()
        self.backgroundColor = UIColor.clearColor()
        self.contentView.backgroundColor = UIColor.clearColor()
        
    }
}
