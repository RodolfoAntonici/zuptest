//
//  MoviesManager.swift
//  ZupTest
//
//  Created by Rodolfo Antonici on 6/30/16.
//  Copyright © 2016 Rodolfo Antonici. All rights reserved.
//

import Alamofire

class MoviesManager {
    
    static let sharedManager = MoviesManager()
    
    func getMoviesWithTitle(title: String!, completionHandler:([Movie]) -> Void, errorHandler:(NSError) -> Void) {
        Alamofire.request(.POST, "https://www.omdbapi.com/", parameters: ["s" : title, "r" : "json", "type" : "Movie"], encoding: ParameterEncoding.URLEncodedInURL, headers: nil).responseJSON { (response) in
            
            guard let value = response.result.value,
            let searchResult = value["Search"] as? [[String: AnyObject]]
            where response.result.error == nil else{
                //Damn, we got an error!
                if let error = response.result.error {
                    errorHandler(error)
                }
                else {
                    //Otherwise we didn't found a thing
                    completionHandler([Movie]())
                }
                
                
                return
            }
            
            var movieArray = [Movie]()
            for movieDictionary in searchResult {
                // The year conversion is bizzare, but for some reason you can't concatenate the conversion with the type cast
                // Why not leave it as string? Well, if it's needed for a query a string could mess things up
                guard let poster = movieDictionary["Poster"] as? String,
                    let title = movieDictionary["Title"] as? String,
                    let yearString = movieDictionary["Year"] as? String,
                    let year = Int(yearString),
                    let imdbID = movieDictionary["imdbID"] as? String
                    else {
                        //If any of these values are nil, it should skip the movie (keeping things simple)
                    continue
                }
                
                let currentMovie = Movie()
                currentMovie.posterURLString = poster == "N/A" ? nil : poster
                currentMovie.title = title
                currentMovie.year = year
                currentMovie.imdbID = imdbID
                
                movieArray.append(currentMovie)
                
            }
            
            completionHandler(movieArray)
        
        }
    }
    
    func getMovieWithImdbID(imdbID: String!, completionHandler:(Movie?) -> Void, errorHandler:(NSError) -> Void) {
        Alamofire.request(.POST, "https://www.omdbapi.com/", parameters: ["i" : imdbID, "r" : "json", "type" : "Movie", "plot" : "full"], encoding: ParameterEncoding.URLEncodedInURL, headers: nil).responseJSON { (response) in
            
            guard let movieDictionary = response.result.value as? [String: AnyObject]
                where response.result.error == nil else{
                    //Damn, we got an error!
                    if let error = response.result.error {
                        errorHandler(error)
                    }
                    else {
                        //Otherwise we didn't found a thing
                        completionHandler(nil)
                    }
                    
                    
                    return
            }
            
            
            
        if let poster = movieDictionary["Poster"] as? String,
            let title = movieDictionary["Title"] as? String,
            let yearString = movieDictionary["Year"] as? String,
            let year = Int(yearString),
            let imdbID = movieDictionary["imdbID"] as? String
         {
                
            let currentMovie = Movie()
            currentMovie.posterURLString = poster == "N/A" ? nil : poster
            currentMovie.title = title
            currentMovie.year = year
            currentMovie.imdbID = imdbID
            
            if let genre = movieDictionary["Genre"] as? String {
                currentMovie.genre = genre
            }
            
            if let actors = movieDictionary["Actors"] as? String {
                currentMovie.actors = actors
            }
            
            if let director = movieDictionary["Director"] as? String {
                currentMovie.director = director
            }
            
            if let plot = movieDictionary["Plot"] as? String {
                currentMovie.plot = plot
            }
            completionHandler(currentMovie)
        }
            
            
            
            
        }
    }
    
}
