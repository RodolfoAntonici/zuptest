//
//  UIViewExtension.swift
//  Jeitto
//
//  Created by Rodolfo Antonici on 24/4/16.
//  Copyright © 2016 Rodolfo Antonici. All rights reserved.
//

import UIKit

@IBDesignable
extension UIView {
    @IBInspectable var cornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            self.layer.cornerRadius = newValue
            self.layer.masksToBounds = newValue > 0
        }
    }
    @IBInspectable var borderWidth: CGFloat {
        get {
            return layer.borderWidth
        }
        set {
            self.layer.borderWidth = newValue
        }
    }
    @IBInspectable var borderColor: UIColor? {
        get {
            if let layerColor = self.layer.borderColor {
                return UIColor(CGColor: layerColor)
            }
            else {
                return nil
            }
        }
        set {
            self.layer.borderColor = newValue?.CGColor
        }
    }
    
    
}
