//
//  Movie.swift
//  ZupTest
//
//  Created by Rodolfo Antonici on 6/30/16.
//  Copyright © 2016 Rodolfo Antonici. All rights reserved.
//

import RealmSwift

class Movie: Object {
//    To get things simple: the database will use the same primary key as the imdbID, so if it's needed to fetch any aditional info it's really easy and it guarantee a duplicated free database
    dynamic var imdbID = ""
    dynamic var posterURLString: String?
    dynamic var title = ""
    dynamic var year = 0
    dynamic var genre: String?
    dynamic var director: String?
    dynamic var actors: String?
    dynamic var plot: String?
    
    override static func primaryKey() -> String? {
        return "imdbID"
    }
}
