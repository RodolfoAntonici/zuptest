//
//  UIViewControllerExtension.swift
//  Jeitto
//
//  Created by Rodolfo Antonici on 24/4/16.
//  Copyright © 2016 Rodolfo Antonici. All rights reserved.
//

import UIKit



extension UIViewController {
    
    func clearNavigationBackButton()  {
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.Plain, target:nil, action:nil)
    }
}
