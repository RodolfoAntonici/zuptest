//
//  MovieSearchTableViewController.swift
//  ZupTest
//
//  Created by Rodolfo Antonici on 6/30/16.
//  Copyright © 2016 Rodolfo Antonici. All rights reserved.
//

import UIKit

private let MovieCellReuseIdentifier = "MovieCell"
private let MovieDetailSegueIdentifier = "MovieDetail"
class MovieSearchTableViewController: UITableViewController {
    private var searchResults = [Movie]()
    
    lazy var searchBar: UISearchBar = {
        let searchBar = UISearchBar(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 44))
        searchBar.delegate = self
        
        return searchBar
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.clearNavigationBackButton()
       
    }
    
    func performSearchWithSearchBarString() {
        print(self.searchBar.text)
        
        guard let searchBarText = self.searchBar.text where !searchBarText.isEmpty else {
            self.searchResults = [Movie]()
            self.tableView!.reloadData()
            return
        }
        MoviesManager.sharedManager.getMoviesWithTitle(searchBarText, completionHandler: { (movies) in
            self.searchResults = movies
            self.tableView!.reloadData()
        }) { (error) in
            
        }
    }

    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        guard let segueIdentifier = segue.identifier else {
            return
        }
        switch segueIdentifier {
        case MovieDetailSegueIdentifier:
            let movieDetailViewController = segue.destinationViewController as! MovieDetailViewController
            movieDetailViewController.movie = sender as? Movie
        default:
            break
        }
    }
    
    
    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.searchResults.count
    }

    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier(MovieCellReuseIdentifier, forIndexPath: indexPath)
        
        let movie = self.searchResults[indexPath.row]
        cell.textLabel?.text = movie.title
        cell.detailTextLabel?.text = "\(movie.year)"

        return cell
    }
    
    
    //MARK - Table view delegate
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {

        self.performSegueWithIdentifier("MovieDetail", sender: self.searchResults[indexPath.row])
    }

    override func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 44
    }
    
    override func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return self.searchBar
    }

}

extension MovieSearchTableViewController: UISearchBarDelegate {
    func searchBar(searchBar: UISearchBar, textDidChange searchText: String) {
        
        NSObject.cancelPreviousPerformRequestsWithTarget(self, selector: #selector(MovieSearchTableViewController.performSearchWithSearchBarString), object: nil)
        
        self.performSelector(#selector(MovieSearchTableViewController.performSearchWithSearchBarString), withObject: nil, afterDelay: 0.5)
    }
    
    func searchBarSearchButtonClicked(searchBar: UISearchBar) {
        NSObject.cancelPreviousPerformRequestsWithTarget(self, selector: #selector(MovieSearchTableViewController.performSearchWithSearchBarString), object: nil)
        
        self.performSelector(#selector(MovieSearchTableViewController.performSearchWithSearchBarString), withObject: nil, afterDelay: 0.5)
    }
}
